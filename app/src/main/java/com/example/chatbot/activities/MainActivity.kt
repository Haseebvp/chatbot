package com.example.chatbot.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.chatbot.R
import com.example.chatbot.database.DbHelper
import com.example.chatbot.fragments.ChatHistoryFragment
import com.example.chatbot.fragments.ChatScreenFragment
import com.example.chatbot.utils.AppUtils
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import java.text.SimpleDateFormat


class MainActivity : AppCompatActivity() {

    lateinit var currentFragment: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar.title = getString(R.string.app_name)

        currentFragment = ChatHistoryFragment()
        setFragment(currentFragment)

        val db = DbHelper(this@MainActivity, null)
        fbNewChat.setOnClickListener(View.OnClickListener {
            currentFragment = ChatScreenFragment()
            val name = random();
            val id = db.addGroup(name)
            val bundle = Bundle()
            bundle.putLong(AppUtils.GROUP_ID, id)
            bundle.putString(AppUtils.GROUP_NAME, name)
            currentFragment.arguments = bundle
            setFragment(currentFragment)
            updateFab(currentFragment)
        })
    }

    fun updateFab(fragment: Fragment) {
        if (fragment is ChatHistoryFragment) {
            fbNewChat.visibility = View.VISIBLE
        } else {
            fbNewChat.visibility = View.GONE
        }
    }

    fun setFragment(fragment: Fragment) {
        val fm = supportFragmentManager
        val fragmentTransaction = fm.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentHolder, fragment)
        if (fragment is ChatScreenFragment) {
            fragmentTransaction.addToBackStack(fragment.javaClass.simpleName)
        }
        fragmentTransaction.commit()
    }

    fun random(): String {
        val pattern = "MM/dd/yyyy HH:mm:ss"
        val df = SimpleDateFormat(pattern)
        val today = Calendar.getInstance().time
        val todayAsString = df.format(today)
        return AppUtils.USER + todayAsString
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        if (count == 0) {
            super.onBackPressed()
        } else {
            supportFragmentManager.popBackStack()
            updateFab(currentFragment)
            updateTitle(resources.getString(R.string.app_name))
        }
    }

    fun updateTitle(grouP_NAME: String?) {
        toolbar.title = grouP_NAME
    }
}
