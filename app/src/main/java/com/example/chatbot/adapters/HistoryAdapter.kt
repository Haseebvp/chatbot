package com.example.chatbot.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.chatbot.R
import com.example.chatbot.models.HistoryGroup
import kotlinx.android.synthetic.main.item_group.view.*
import com.amulyakhare.textdrawable.TextDrawable
import android.graphics.Color
import android.os.Bundle
import com.example.chatbot.activities.MainActivity
import com.example.chatbot.fragments.ChatScreenFragment
import com.example.chatbot.utils.AppUtils
import java.lang.Exception


class HistoryAdapter(val context: Context, val data: ArrayList<HistoryGroup>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val holder: RecyclerView.ViewHolder = GroupViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_group,
                parent,
                false
            )
        )

        return holder
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = data[position]
        (holder as GroupViewHolder).setUp(item, context)
    }

}

class GroupViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun setUp(item: HistoryGroup, context: Context) {
        try {
            itemView.tvTitle.text = item.title
            val drawable = TextDrawable.builder()
                .buildRect(item.title[0].toString().toUpperCase(), Color.DKGRAY)
            itemView.ivProfile.setImageDrawable(drawable)
            itemView.setOnClickListener(View.OnClickListener {
                val fragment = ChatScreenFragment()
                val bundle = Bundle()
                bundle.putLong(AppUtils.GROUP_ID, item.id)
                bundle.putString(AppUtils.GROUP_NAME, item.title)
                fragment.arguments = bundle
                (context as MainActivity).setFragment(fragment)
                (context as MainActivity).updateFab(fragment)
            })
        } catch (e: Exception) {

        }
    }

}
