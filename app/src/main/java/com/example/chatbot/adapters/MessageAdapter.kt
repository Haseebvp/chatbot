package com.example.chatbot.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.chatbot.R
import com.example.chatbot.adapters.viewHolders.RecievedViewHolder
import com.example.chatbot.adapters.viewHolders.SentViewHolder
import com.example.chatbot.models.ChatMessage

class MessageAdapter(val context: Context, val dataList: ArrayList<ChatMessage>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TYPE_SENT = 0
        const val TYPE_RECIEVED = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val holder: RecyclerView.ViewHolder
        when (viewType) {
            TYPE_SENT -> holder = SentViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_sent,
                    parent,
                    false
                )
            )
            TYPE_RECIEVED -> holder = RecievedViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_recieved,
                    parent,
                    false
                )
            )

            else ->
                holder = SentViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_sent,
                        parent,
                        false
                    )
                )
        }
        return holder
    }

    override fun getItemCount(): Int {
        return dataList.size
    }


    override fun getItemViewType(position: Int): Int {
        return dataList[position].type
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item: ChatMessage = dataList[position]
        if ((holder.itemViewType == TYPE_SENT)) {
            (holder as SentViewHolder).setUp(item, context)
        } else if (holder.itemViewType == TYPE_RECIEVED) {
            (holder as RecievedViewHolder).setUp(item, context)
        }
    }

}