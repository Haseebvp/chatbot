package com.example.chatbot.adapters.viewHolders

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.chatbot.models.ChatMessage

abstract class BaseViewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    abstract fun setUp(item: ChatMessage, context: Context)
}
