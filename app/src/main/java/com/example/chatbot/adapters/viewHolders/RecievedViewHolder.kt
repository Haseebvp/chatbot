package com.example.chatbot.adapters.viewHolders

import android.content.Context
import android.view.View
import com.example.chatbot.models.ChatMessage
import kotlinx.android.synthetic.main.item_recieved.view.*

class RecievedViewHolder(itemView: View) : BaseViewholder(itemView) {
    override fun setUp(item: ChatMessage, context: Context) {
        itemView.tvReceivedMessage.text = item.message
    }

}
