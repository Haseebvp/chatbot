package com.example.chatbot.adapters.viewHolders

import android.content.Context
import android.os.Build
import android.os.Handler
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.chatbot.R
import com.example.chatbot.adapters.MessageAdapter
import com.example.chatbot.models.ChatMessage
import kotlinx.android.synthetic.main.item_sent.view.*
import java.lang.Exception

class SentViewHolder(itemView: View) : BaseViewholder(itemView) {
    override fun setUp(item: ChatMessage, context: Context) {
        try {
            itemView.tvSentMessage.text = item.message
//            if (item.isPending == 1){
//                itemView.tvSentMessage
//            }
        } catch (e: Exception) {

        }

    }

}
