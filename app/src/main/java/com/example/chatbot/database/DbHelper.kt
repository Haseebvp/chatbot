package com.example.chatbot.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.chatbot.models.ChatMessage
import com.example.chatbot.models.HistoryGroup
import java.lang.Exception


class DbHelper(
    context: Context,
    factory: SQLiteDatabase.CursorFactory?
) :
    SQLiteOpenHelper(
        context, DATABASE_NAME,
        factory, DATABASE_VERSION
    ) {
    override fun onCreate(db: SQLiteDatabase) {
        val CREATE_MESSAGES_TABLE = ("CREATE TABLE IF NOT EXISTS " +
                TABLE_MESSAGE_NAME + "("
                + COLUMN_MESSAGE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_MESSAGE_CONTENT + " TEXT," +
                COLUMN_MESSAGE_CHATBOTID + " TEXT," +
                COLUMN_GROUP + " INTEGER," +
                COLUMN_ISPENDING + " INTEGER," +
                COLUMN_MESSAGE_TYPE + " INTEGER" + ")")
        db.execSQL(CREATE_MESSAGES_TABLE)
        val CREATE_GROUPS_TABLE = ("CREATE TABLE IF NOT EXISTS " +
                TABLE_GROUP_NAME + "("
                + COLUMN_GROUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_GROUP_TITLE + " TEXT" + ")")
        db.execSQL(CREATE_GROUPS_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUP_NAME)
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGE_NAME)
        onCreate(db)
    }

    fun addMessage(message: ChatMessage, group: Long) {
        val values = ContentValues()
        values.put(COLUMN_MESSAGE_CONTENT, message.message)
        values.put(COLUMN_MESSAGE_CHATBOTID, message.chatbotId)
        values.put(COLUMN_MESSAGE_TYPE, message.type)
        values.put(COLUMN_GROUP, group)
        values.put(COLUMN_ISPENDING, message.isPending)
        val db = this.writableDatabase
        db.insert(TABLE_MESSAGE_NAME, null, values)
        db.close()
    }

    fun addGroup(title: String): Long {
        val values = ContentValues()
        values.put(COLUMN_GROUP_TITLE, title)
        val db = this.writableDatabase
        val id = db.insert(TABLE_GROUP_NAME, null, values)
        db.close()
        return id
    }

    fun updateMessage(item: ChatMessage) {
        val values = ContentValues()
        values.put(COLUMN_MESSAGE_CONTENT, item.message)
        values.put(COLUMN_MESSAGE_CHATBOTID, item.chatbotId)
        values.put(COLUMN_MESSAGE_TYPE, item.type)
        values.put(COLUMN_GROUP, item.groupId)
        values.put(COLUMN_ISPENDING, false)
        val db = this.writableDatabase
        db.update(TABLE_MESSAGE_NAME, values, "_id = ?", arrayOf(item._id.toString()))
        db.close()
    }


    fun getAllGroups(): ArrayList<HistoryGroup> {
        val dataList = ArrayList<HistoryGroup>()
        val db = this.readableDatabase
        val cursor: Cursor = db.rawQuery("SELECT * FROM $TABLE_GROUP_NAME ORDER BY $COLUMN_GROUP_ID DESC", null)
        if (cursor.moveToFirst()) {
            do {
                val id = cursor.getLong(cursor.getColumnIndex(COLUMN_GROUP_ID))
                val title = cursor.getString(cursor.getColumnIndex(COLUMN_GROUP_TITLE))
                val grp = HistoryGroup(id, title)
                dataList.add(grp)
            } while (cursor.moveToNext())
        }

        // close db connection
        db.close()
        return dataList
    }

    fun getPendingChats(): ArrayList<ChatMessage> {
        val dataList = ArrayList<ChatMessage>()
        val db = this.readableDatabase
        try {
            val cursor: Cursor = db.rawQuery("SELECT * FROM $TABLE_MESSAGE_NAME WHERE $COLUMN_ISPENDING = 1", null)
            if (cursor.moveToFirst()) {
                do {
                    val id = cursor.getInt(cursor.getColumnIndex(COLUMN_MESSAGE_ID))
                    val message = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_CONTENT))
                    val botid = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_CHATBOTID))
                    val type = cursor.getInt(cursor.getColumnIndex(COLUMN_MESSAGE_TYPE))
                    val group = cursor.getLong(cursor.getColumnIndex(COLUMN_GROUP))
                    val ispending = cursor.getInt(cursor.getColumnIndex(COLUMN_ISPENDING))
                    val messageObj = ChatMessage(message, botid, type)
                    messageObj.groupId = group
                    messageObj.isPending = ispending
                    messageObj._id = id
                    dataList.add(messageObj)
                } while (cursor.moveToNext())
            }
        } catch (e: Exception) {
            println("hhhh")
        }
        db.close()
        return dataList
    }


    fun getMessageList(groupId: Long): ArrayList<ChatMessage> {
        val dataList = ArrayList<ChatMessage>()
        val db = this.readableDatabase
        try {
            val cursor: Cursor = db.rawQuery("SELECT * FROM $TABLE_MESSAGE_NAME WHERE $COLUMN_GROUP = $groupId", null)
            if (cursor.moveToFirst()) {
                do {
                    val id = cursor.getInt(cursor.getColumnIndex(COLUMN_MESSAGE_ID))
                    val message = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_CONTENT))
                    val botid = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_CHATBOTID))
                    val type = cursor.getInt(cursor.getColumnIndex(COLUMN_MESSAGE_TYPE))
                    val group = cursor.getLong(cursor.getColumnIndex(COLUMN_GROUP))
                    val ispending = cursor.getInt(cursor.getColumnIndex(COLUMN_ISPENDING))
                    val messageObj = ChatMessage(message, botid, type)
                    messageObj.groupId = group
                    messageObj.isPending = ispending
                    dataList.add(messageObj)
                } while (cursor.moveToNext())
            }
        } catch (e: Exception) {
            println("hhhh")
        }
        db.close()
        return dataList
    }

    companion object {
        private val DATABASE_VERSION = 9
        private val DATABASE_NAME = "chatbot.db"
        val TABLE_MESSAGE_NAME = "message"
        val COLUMN_MESSAGE_ID = "_id"
        val COLUMN_MESSAGE_CONTENT = "content"
        val COLUMN_MESSAGE_CHATBOTID = "chatbotid"
        val COLUMN_MESSAGE_TYPE = "type"
        val COLUMN_GROUP = "groupid"
        val COLUMN_ISPENDING = "ispending"

        val TABLE_GROUP_NAME = "grouptable"
        val COLUMN_GROUP_ID = "_id"
        val COLUMN_GROUP_TITLE = "title"

    }
}