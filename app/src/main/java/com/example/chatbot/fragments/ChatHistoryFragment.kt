package com.example.chatbot.fragments

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.chatbot.R
import com.example.chatbot.adapters.HistoryAdapter
import com.example.chatbot.database.DbHelper
import com.example.chatbot.models.HistoryGroup
import kotlinx.android.synthetic.main.fragment_chat_history.view.*
import java.lang.Exception

class ChatHistoryFragment : Fragment() {

    lateinit var adapter: HistoryAdapter
    lateinit var recyclerView: RecyclerView
    var data = ArrayList<HistoryGroup>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat_history, container, false);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.historyList
        adapter = HistoryAdapter(this.context!!, data)
        recyclerView.layoutManager = LinearLayoutManager(this.activity, RecyclerView.VERTICAL, false)
        recyclerView.adapter = adapter
        val dividerItemDecoration = DividerItemDecoration(this.context, RecyclerView.VERTICAL)
        if (Build.VERSION.SDK_INT >= 22) {
            dividerItemDecoration.setDrawable(context!!.resources.getDrawable(R.drawable.line_divider))
        }else{
            dividerItemDecoration.setDrawable(ContextCompat.getDrawable(this.context!!, R.drawable.line_divider)!!)
        }
        recyclerView.addItemDecoration(dividerItemDecoration)
        data.clear()
        fetchData()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun fetchData() {
        try {
            val db = DbHelper(this.context!!, null)
            data.addAll(db.getAllGroups())
            adapter.notifyDataSetChanged()
        } catch (e: Exception) {

        }

    }
}