package com.example.chatbot.fragments

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.chatbot.R
import com.example.chatbot.activities.MainActivity
import com.example.chatbot.adapters.MessageAdapter
import com.example.chatbot.database.DbHelper
import com.example.chatbot.models.ChatMessage
import com.example.chatbot.utils.AppUtils
import kotlinx.android.synthetic.main.fragment_chat_screen.*
import org.greenrobot.eventbus.EventBus
import java.lang.Exception
import org.greenrobot.eventbus.ThreadMode
import org.greenrobot.eventbus.Subscribe
import com.example.chatbot.utils.messages.UpdateListEvent


class ChatScreenFragment : Fragment() {
    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: MessageAdapter
    var dataList: ArrayList<ChatMessage> = ArrayList()
    lateinit var handler: Handler
    var GROUP_ID = 0L
    var GROUP_NAME = ""
    lateinit var dbHelper: DbHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = this.arguments
        if (bundle != null) {
            GROUP_ID = bundle.getLong(AppUtils.GROUP_ID, 0)
            GROUP_NAME  = bundle.getString(AppUtils.GROUP_NAME)
            (this.activity as MainActivity).updateTitle(GROUP_NAME)
        }
        handler = Handler()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat_screen, container, false);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dbHelper = DbHelper(this.context!!, null)
        adapter = MessageAdapter(this.activity!!, dataList)
        layoutManager = LinearLayoutManager(this.activity, RecyclerView.VERTICAL, false)
        layoutManager.stackFromEnd = true
        recyclerview.layoutManager = layoutManager
        recyclerview.adapter = adapter
        recyclerview.setHasFixedSize(true)
        recyclerview.addOnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            if (bottom < oldBottom) {
                recyclerview.postDelayed(Runnable {
                    recyclerview.scrollToPosition(adapter.itemCount)
                }, 100)
            }
        }
        fetchData()
        ivSend.setOnClickListener(View.OnClickListener {
            if (et_message.text.toString().trim().isNotEmpty()) {
                AppUtils.postComment(et_message.text.toString(), this.context!!, false,GROUP_ID)
            }
        })
    }

    private fun fetchData() {
        val msg: ArrayList<ChatMessage> = dbHelper.getMessageList(GROUP_ID)
        dataList.addAll(msg)
        adapter.notifyDataSetChanged()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: UpdateListEvent) {
        updatelist(event.message, event.chatbotid, event.type, event.isPending)
    }

    private fun updatelist(message: String, chatbotid: Long?, type: Int, isPending:Int) {
        try {
            val chatMessage = if (chatbotid != null) {
                ChatMessage(message, chatbotid.toString(), type)
            } else {
                ChatMessage(message, AppUtils.CHAT_BOTID.toString(), type)
            }
            chatMessage.isPending = isPending

            dataList.add(chatMessage)
            adapter.notifyItemInserted(dataList.size - 1)
            handler.postDelayed({
                recyclerview.scrollToPosition(dataList.size - 1)
            }, 200)
            dbHelper.addMessage(chatMessage, GROUP_ID)
            et_message.setText("")
        } catch (e: Exception) {
            println("kkk")
        }
    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }


}