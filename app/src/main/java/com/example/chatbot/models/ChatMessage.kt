package com.example.chatbot.models


data class ChatMessage(val message: String, val chatbotId: String, var type: Int) {
    var groupId: Long = 0L
    var isPending: Int = 0
    var _id:Int = 0
}
