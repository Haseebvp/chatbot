package com.example.chatbot.models

data class ChatMessageInner(
    val chatBotName: String,
    val chatBotID: Long,
    val message: String,
    val emotion: String
)
