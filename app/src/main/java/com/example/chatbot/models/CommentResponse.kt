package com.example.chatbot.models

data class CommentResponse(
    val success: Int,
    val errorMessage: String,
    val message: ChatMessageInner,
    val data: ArrayList<String>
)
