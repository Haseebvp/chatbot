package com.example.chatbot.network

import com.example.chatbot.utils.AppUtils
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object ApiFactory{
    fun create(): ApiService {
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(AppUtils.BASE_URL)
            .build()

        return retrofit.create(ApiService::class.java);
    }

}