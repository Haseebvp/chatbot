package com.example.chatbot.network

import com.example.chatbot.models.CommentResponse
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("api/chat")
    fun postComment(
        @Query("apiKey") apikey: String,
        @Query("message") message: String,
        @Query("chatBotID") botid: String,
        @Query("externalID") externalid: String
    ): Observable<CommentResponse>
}