package com.example.chatbot.tasks

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.chatbot.database.DbHelper
import com.example.chatbot.utils.AppUtils

class OnlineSyncWorker(private val ctx: Context, params: WorkerParameters) : Worker(ctx, params) {
    override fun doWork(): Result {
        val dbHelper = DbHelper(ctx, null)
        val pendingList = dbHelper.getPendingChats()
        if (pendingList.size > 0){
            for (i in 0 until pendingList.size){
                val item = pendingList[i]
                AppUtils.postComment(item.message, ctx, true, item.groupId)
                dbHelper.updateMessage(item)
            }
        }
        return Result.success()
    }

}