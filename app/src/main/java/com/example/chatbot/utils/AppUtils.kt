package com.example.chatbot.utils

import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast
import androidx.work.*
import com.example.chatbot.R
import com.example.chatbot.adapters.MessageAdapter
import com.example.chatbot.network.ApiFactory
import com.example.chatbot.network.ApiService
import com.example.chatbot.tasks.OnlineSyncWorker
import com.example.chatbot.utils.messages.UpdateListEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus
import android.content.ComponentName
import android.app.ActivityManager
import android.os.Build
import com.example.chatbot.database.DbHelper
import com.example.chatbot.models.ChatMessage


object AppUtils {
    const val API_KEY: String = "6nt5d1nJHkqbkphe"
    const val CHAT_BOTID: Long = 63906
    const val USER = "Haseeb"
    const val GROUP_ID = "GROUP_ID"
    const val BASE_URL = "https://www.personalityforge.com/"
    const val GROUP_NAME = "groupname"
    const val WORK_NAME = "syncChat"

    fun isInternetAvailable(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return cm!!.activeNetworkInfo != null
    }

    fun scheduleMessageSync() {
        val workManager: WorkManager = WorkManager.getInstance()
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val backupWorkRequest = OneTimeWorkRequestBuilder<OnlineSyncWorker>()
            .setConstraints(constraints)
            .build()
        workManager.enqueueUniqueWork(WORK_NAME, ExistingWorkPolicy.REPLACE, backupWorkRequest)
    }

    fun postComment(message: String, context: Context, isfromworker: Boolean, groupid:Long) {
        if (AppUtils.isInternetAvailable(context)) {
            if (!isfromworker) {
                broadcastEvent(message, null, MessageAdapter.TYPE_SENT, 0, context)
            }
            val apiService: ApiService = ApiFactory.create()
            apiService.postComment(AppUtils.API_KEY, message, AppUtils.CHAT_BOTID.toString(), AppUtils.USER)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    broadcastEvent(result.message.message, result.message.chatBotID, MessageAdapter.TYPE_RECIEVED, 0, context)
                    if (isAppIsInBackground(context)){
                        val dbHelper = DbHelper(context, null)
                        val chatMessage=ChatMessage(message, CHAT_BOTID.toString(), MessageAdapter.TYPE_RECIEVED)
                        chatMessage.isPending = 0
                        dbHelper.addMessage(chatMessage,groupid)
                    }
                }, { error ->
                    error.printStackTrace()
                })
        } else {
            Toast.makeText(context, context.getString(R.string.internet_error_message), Toast.LENGTH_SHORT).show()
            broadcastEvent(message, null, MessageAdapter.TYPE_SENT, 1, context)
            AppUtils.scheduleMessageSync()
        }
    }

    private fun broadcastEvent(message: String, chatid: Long?, type: Int, isPending: Int, context: Context) {
        val event = UpdateListEvent()
        event.message = message
        event.chatbotid = chatid
        event.type = type
        event.isPending = isPending
        EventBus.getDefault().post(event)

    }

    private fun isAppIsInBackground(context: Context): Boolean {
        var isInBackground = true
        val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            val runningProcesses = am.runningAppProcesses
            for (processInfo in runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (activeProcess in processInfo.pkgList) {
                        if (activeProcess == context.packageName) {
                            isInBackground = false
                        }
                    }
                }
            }
        } else {
            val taskInfo = am.getRunningTasks(1)
            val componentInfo = taskInfo[0].topActivity
            if (componentInfo.packageName == context.packageName) {
                isInBackground = false
            }
        }

        return isInBackground
    }


}