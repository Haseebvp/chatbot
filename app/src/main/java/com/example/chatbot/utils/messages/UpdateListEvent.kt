package com.example.chatbot.utils.messages

import com.example.chatbot.adapters.MessageAdapter
import com.example.chatbot.utils.AppUtils

class UpdateListEvent{
    var message: String = ""
    var chatbotid: Long? = AppUtils.CHAT_BOTID
    var type: Int = MessageAdapter.TYPE_SENT
    var isPending:Int = 0
}